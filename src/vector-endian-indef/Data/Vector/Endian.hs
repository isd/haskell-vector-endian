module Data.Vector.Endian
    ( MVector
    , Vector

    -- * Accessors
    -- ** Length information
    , length
    , null
    -- ** Indexing
    , (!)
    , (!?)
    , head
    , last
    , unsafeIndex
    , unsafeHead
    , unsafeLast
    -- ** Monadic indexing
    , indexM
    , headM
    , lastM
    , unsafeIndexM
    , unsafeHeadM
    , unsafeLastM
    -- ** Extracting subvectors (slicing)
    , slice
    , init
    , tail
    , take
    , drop
    , splitAt
    , unsafeSlice
    , unsafeInit
    , unsafeTail
    , unsafeTake
    , unsafeDrop
    -- * Construction
    -- ** Initialisation
    , empty
    , singleton
    , replicate
    , generate
    , iterateN
    -- ** Monadic initialisation
    , replicateM
    , generateM
    , iterateNM
    , create
    , createT
    -- ** Unfolding
    , unfoldr
    , unfoldrN
    , unfoldrM
    , unfoldrNM
    , constructN
    , constructrN
    -- ** Enumeration
    , enumFromN
    , enumFromStepN
    , enumFromTo
    , enumFromThenTo
    -- * Raw pointers
    , unsafeFromForeignPtr
    , unsafeFromForeignPtr0
    , unsafeToForeignPtr
    , unsafeToForeignPtr0
    , unsafeWith
    ) where

import Internal.Vector
