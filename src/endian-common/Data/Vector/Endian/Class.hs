module Data.Vector.Endian.Class where

import System.Endian
import Zhp

class Endian a where
    fromLE :: a -> a
    toLE :: a -> a
    fromBE :: a -> a
    toBE :: a -> a

instance Endian Word8 where
    fromLE = id
    toLE = id
    fromBE = id
    toBE = id

instance Endian Word16 where
    fromLE = fromLE16
    toLE = toLE16
    fromBE = fromBE16
    toBE = toBE16

instance Endian Word32 where
    fromLE = fromLE32
    toLE = toLE32
    fromBE = fromBE32
    toBE = toBE32

instance Endian Word64 where
    fromLE = fromLE64
    toLE = toLE64
    fromBE = fromBE64
    toBE = toBE64
