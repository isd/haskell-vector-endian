module Internal.Endianness (to, from) where

import Data.Vector.Endian.Class

from, to :: Endian a => a -> a
from = fromBE
to = toBE
