This is a Haskell package for working with flat arrays of fixed-size
numeric values in a format that is independent of host CPU endianness
(or word size).  This means that you can manipulate the array in memory,
and then just write it out to disk or the network when finished -- no
marshalling step.

The types exposed by this package can be used with the `vector` package;
they implement the type classes provided by that package.
